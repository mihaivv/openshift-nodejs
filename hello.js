var http = require('http');
var mdt = require('./mydatetimemodule');

console.log("The server is started");
http.createServer(function (req, res) {
  console.log("New Request Received : " + mdt.myDateTime());
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('Hallo 4 from js!');
}).listen(8080);